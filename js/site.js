//Window Onload
(function () {
    document.body.style.overflow = 'hidden';

	setTimeout(function(){ 
        document.body.style.overflow = 'visible';
        document.body.style.overflowX = 'hidden';
        document.getElementById('loadingOverlay').classList.add('fadeOut');
    }, 1500);
})();

//Audio play button
let audio, playbtn, dir, ext, playlist, playlist_status, playlist_index, agent;

dir="audio/";
playlist = ['hurting'];

ext = ".mp3";
agent = navigator.userAgent.toLowerCase();
if(agent.indexOf('firefix') != -1 || agent.indexOf('opera') != -1){
    ext = ".ogg";
}

playbtn = document.getElementById('playpausebtn');
playlist_index = 0;

audio =  new Audio();
audio.src = dir + playlist[0] + ext;
audio.loop = false;

playbtn.addEventListener('click', playPause);

function fetchMusicDetails(){
    document.querySelector('.play-pause i').classList = 'fas fa-pause';
    audio.src = dir+playlist[playlist_index]+ext;
    audio.play();
}

function playPause(){
    if(audio.paused){
        audio.play();
        document.querySelector('.play-pause i').classList = 'fas fa-pause';
    }
    else{
        audio.pause();
        document.querySelector('.play-pause i').classList = 'fas fa-play';
    }
}

//Parallax
const parallaxHeader = document.getElementById('headerLax');

window.addEventListener('scroll', function(){
    let offset =  window.pageYOffset;
    parallaxHeader.style.backgroundPositionY = offset * 0.7 + 'px';
});

//Side Nav / Burger Btn
const mySidenav = document.getElementById('mySidenav');
const burgerBtn = document.getElementById('burgerBtn');
const closeBtn = document.getElementById('closeBtn');

function openNav() {
    document.getElementById("mySidenav").style.width = "320px";
    document.getElementById("mySidenav").style.opacity = "1";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("mySidenav").style.opacity = "0";
}

burgerBtn.addEventListener('click', openNav);
closeBtn.addEventListener('click', closeNav);

$(document).ready(function() {
    $('#mySidenav a[href^="#"]').on('click', function(e){
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').animate({
            'scrollTop': $target.offset().top - 75
        }, 1500, 'swing');

        closeNav();
    });

    //---Back to Top button
    $('#toTop').on('click', function(e){
        $('html, body').animate({ scrollTop: '0' }, 2000);
    });
});

//Gallery Load More Button
var loadMore = document.getElementById('galleryLoadMore');
var galleryHiddenItem = document.querySelectorAll('.items.is-hidden');

loadMore.addEventListener('click', function(){
    galleryHiddenItem.forEach(function(hiddenItem){
        hiddenItem.classList.remove('is-hidden');
    });
    loadMore.classList.add('is-hidden');
});